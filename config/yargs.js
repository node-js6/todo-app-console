const description = {
        demand: true,
        alias: 'd',
        desc: 'Descripcion de la tarea por hacer'
}

const state =  {
        alias: 's',
        demand: false
}

const complete = {
        alias: 'c',
        default: true,
        desc: 'Marca como completado o pendiente una tarea.'
            
}


const argvConfig = require('yargs')
                        .command('create','Opcion la cual permite la creacion de una nueva tarea.',{ description })
                        .command('list', 'Lista todas las tareas segun su estado to-do(td), doing (di), done (do)', {state})
                        .command('update', 'Actualiza una tarea a completada',{description, complete})
                        .command('delete', 'Elimina una tarea previamente creada', {description })
                        .help()
                        .argv;

module.exports  = {
    argvConfig
}
