const { argvConfig } = require('./config/yargs')
const {create, update, list, deleteReg } = require('./note/note')

//console.log(argv)

let comando = argvConfig._[0]
//console.log("comando: " + comando);
console.log(argvConfig.description);


switch( comando ){
    case 'create':
        console.log('argvConfig.description: ' + argvConfig.description);
        let tarea = create(argvConfig.description);
        console.log(tarea);
        break;
    case 'list':
        list(argvConfig.state)
        break
    case 'update':
        update(argvConfig.description, argvConfig.complete)
        break;
    case 'delete':
        let borrado = deleteReg(argvConfig.description);
        console.log(borrado);
        break;
    default:
        console.log('comando no reconocido') 
}