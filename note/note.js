const fs = require('fs')
const colors = require('colors');

let listTODO = [];


const saveDB = () => {

    let data = JSON.stringify(listTODO);
    fs.writeFile('db/data.json',data, (err) => {
        if (err){
            throw new Error('Ocurrio un error al crear el archivo', err);
        }else {
            console.log('Registro guardado exitosamente.')
        }
    });
};

const cargarDB = () => {
    console.log('ejecutando metodo cargarDB...')
    try {
         listTODO = require('../db/data.json');
    } catch (error) {
        console.log('error: ' + error);
         listTODO = [];
    }
   // console.log(listTODO);
};

const create = (description) => {

    
    cargarDB();
    tarea = {
        description,
        complete: false
    };

    listTODO.push(tarea);
    saveDB();

    return listTODO;



};

const deleteReg = (description) => {
    cargarDB();
    let index = listTODO.findIndex( tarea => tarea.description === description );
    console.log(index);
    if (index >= 0){
        listTODO.splice(index,1);
        saveDB();
        return true;
    }else {
        return false;
    }
    //listTODO[index]


};

let list = () => {
    cargarDB()
    console.log('==============                 =============='.green)
    console.log('============== Lista de Tareas =============='.yellow)
    console.log('==============                 =============='.green)
    
    for (let tarea of listTODO){
        console.log(`Tarea:  ${tarea.description} estado: ${tarea.complete}`)
    }

}

let update = (description, complete ) => {
    console.log('Ejecuntado function update params: ' + description);
    cargarDB();
    console.log(listTODO)
    console.log('buscar')


    // let work = listTODO.find( tarea => {
    //             return tarea.description === description 
    // });
    // console.log('work', work);

    let index = listTODO.findIndex( tarea  => tarea.description === description);
    

    console.log(index);

    if ( index >= 0) {
        listTODO[index].complete = complete;
        saveDB();
        return true;
    }else {
        return false;
    }

}





module.exports = {
    create,
    list,
    update,
    deleteReg
}